package com.somospnt.boot.weblogic.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaludoRestController {
    
    @RequestMapping("/saludo")
    public String saludo() {
        return "Hola Mundo!!!";
    }
    
}
