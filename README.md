# Spring Boot para Weblogic 11g con JDK 6 #

Proyecto de ejemplo de como configurar Spring Boot para Weblogic 11g con JDK 6.

### Despliegue ###

* Compilar
* Desplegar por consola de Weblogic

### Testing ###

* http://host:port/boot-weblogic-1.0-SNAPSHOT/saludo ==> "Hola Mundo!!!"
* http://host:port/boot-weblogic-1.0-SNAPSHOT/beans
* http://host:port/boot-weblogic-1.0-SNAPSHOT/metrics